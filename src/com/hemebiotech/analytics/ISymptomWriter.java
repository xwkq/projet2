package com.hemebiotech.analytics;

import java.io.IOException;


/**
 * Write Data to file with output path
 */
public interface ISymptomWriter {

    /**
     * @param output path for the output file
     * @throws IOException
     */
 void write(String output) throws IOException;
}
