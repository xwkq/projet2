package com.hemebiotech.analytics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple brute force implementation
 */
public class ReadSymptomDataFromFile implements ISymptomReader {

    private final String filePath;

    /**
     * @param filePath a full or partial path to file with symptom strings in it, one per line
     */
    public ReadSymptomDataFromFile(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Get symptoms to collection for txt file
     */
    @Override
    public List<String> getSymptoms() throws IOException {
        List<String> symptoms = new ArrayList<>();

        if (filePath != null) {

            // Read file
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line = reader.readLine();
            while (line != null) {
                symptoms.add(line);
                line = reader.readLine();
            }
            reader.close();
        }

        return symptoms;
    }



}
