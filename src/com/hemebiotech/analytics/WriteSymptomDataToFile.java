package com.hemebiotech.analytics;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class WriteSymptomDataToFile implements ISymptomWriter{

    public String pathOutput;
    public List<String> symptoms;
    public TreeMap<String, Integer> map;

    /**
     * @param map
     * @param symptoms
     */
    WriteSymptomDataToFile(TreeMap<String, Integer> map, List<String> symptoms){
        this.symptoms = symptoms;
        this.map = map;
    }

    /**
     * Write symptom to file
     * @param pathOutput full path for output file
     * @throws IOException
     */
    @Override
    public void write(String pathOutput) throws IOException {
        this.pathOutput = pathOutput;
        if (symptoms == null) {
            return;
        }
        // write map to txt file
        try (FileWriter writer = new FileWriter(this.pathOutput)) {
            for (Map.Entry<String, Integer> entry : this.map.entrySet()) {
                writer.write(entry.getKey() + ": " + entry.getValue() + System.lineSeparator());
            }
        }
    }
}
