package com.hemebiotech.analytics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class AnalyticsCounter {
    public static final String DEFAULT_INPUT_PATH = "./symptoms.txt";
    public static final String DEFAULT_OUTPUT_PATH = "./result.txt";
    public List<String> symptoms = new ArrayList<>();
    private String pathSymptoms;
    private String pathOutput;
    public TreeMap<String, Integer> map = new TreeMap<>();

    /**
     * Initialize program and get symptoms
     */
    public void run() {
       this.run(AnalyticsCounter.DEFAULT_INPUT_PATH, AnalyticsCounter.DEFAULT_OUTPUT_PATH);
    }

    /**
     * Initialize program and get symptoms with params
     * @param pathSymptoms Path from the imput file
     * @param pathOutput Path for output file
     */
    public void run(String pathSymptoms, String pathOutput) {
        this.pathSymptoms = pathSymptoms;
        this.pathOutput = pathOutput;
        ReadSymptomDataFromFile readSymptomFromFile = new ReadSymptomDataFromFile(pathSymptoms);
        WriteSymptomDataToFile writeSymptomsToFile = new WriteSymptomDataToFile(map, symptoms);

        try {
            symptoms = readSymptomFromFile.getSymptoms();
            // Get occurrences and symptoms
            analyze();
            // Write symptoms and occurrences to new file
            writeSymptomsToFile.write(pathOutput);
        } catch (IOException e) {
            System.err.println("Cannot read symptoms from the file " + e.getMessage());
            return;
        }
    }


    /**
     * Get symptoms occurences
     */
    private void analyze() {
        for (String symptom : symptoms) {
            // if element exist replace value
            if (map.containsKey(symptom)) {
                map.replace(symptom, map.get(symptom) + 1);
            }else {
                map.put(symptom, 1);
            }
        }
    }

    /**
     * Main
     * */
    public static void main(String args[]) {
        AnalyticsCounter analyticsCounter = new AnalyticsCounter();

        if(args.length == 1){
            // Run program with params
            analyticsCounter.pathSymptoms = args[0];
            analyticsCounter.pathOutput = args[1];
            analyticsCounter.run(analyticsCounter.pathSymptoms, analyticsCounter.pathOutput);

        }else {
            // Run program
            analyticsCounter.run();
        }

    }
}
